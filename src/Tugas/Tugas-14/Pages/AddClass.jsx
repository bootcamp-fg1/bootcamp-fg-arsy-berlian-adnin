import { Form, Input, message, Modal, Radio } from "antd";
import TextArea from "antd/es/input/TextArea";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useState } from "react";

const AddClass = ({ open, setOpen, reload }) => {
  const [form] = Form.useForm();
  const handleSubmit = async () => {
    const values = await form.validateFields();
    try {
      const { data } = await axios.post(`https://thankful-calf-bonnet.cyclic.app/api/classes`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      message.success(data.message);
      reload();
      setOpen(false);
    } catch (error) {
      console.log(error);
    }
  };

  //   ---------------------

  return (
    <Modal open={open} okText="Submit" onOk={handleSubmit} onCancel={() => setOpen(false)} closable={false}>
      <Form form={form} layout="vertical">
        <Form.Item name="name" label="Name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Description" rules={[{ required: true }]}>
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item name="active" label="active">
          <Radio.Group>
            <Radio value={0}>No</Radio>
            <Radio value={1}>Yes</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default AddClass;
