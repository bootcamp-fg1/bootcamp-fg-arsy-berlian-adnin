import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Navigation from "../Components/Navigation";
import "./ClassDetail.css";

const ClassDetail = () => {
  const { id_class } = useParams();
  const [detail, setDetail] = useState([]);
  const { REACT_APP_API: BASE_URL } = process.env;

  const getData = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/classes/${id_class}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      await setDetail(data.data);
      console.log(detail);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <Navigation />
      <div className="detail-class">
        <div className="detail-wrapper">
          <h1>{detail.name}</h1>
          <p>{detail.description}</p>
        </div>
      </div>
    </>
  );
};

export default ClassDetail;
