import { Button, Form, Input, message } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";

const Login = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const handleSubmit = async () => {
    setLoading(true);
    const values = await form.validateFields();
    try {
      const { data } = await axios.post(`https://thankful-calf-bonnet.cyclic.app/api/login`, values);
      message.success(data.message);
      navigate("/");
      Cookies.set("token", data.token.token);
    } catch (error) {
      if (error.message === "Request failed with status code 400") message.warning("harap periksa kembali password anda!");
      else message.warning("periksa kembali email dan password anda!");
    }
    setLoading(false);
  };

  return (
    <div className="login">
      <Form form={form} className="form-login" layout="vertical">
        <h1>Login</h1>
        <p>login to access berlian bootcamp</p>
        <Form.Item name="email" label="" rules={[{ required: true, type: "email" }]}>
          <Input placeholder="email address" />
        </Form.Item>
        <Form.Item name="password" label="" rules={[{ required: true }]}>
          <Input.Password placeholder="password" />
        </Form.Item>
        <Button loading={loading} className="btn-login" type="primary" onClick={handleSubmit} block size="large">
          Login
        </Button>
      </Form>
    </div>
  );
};

export default Login;
