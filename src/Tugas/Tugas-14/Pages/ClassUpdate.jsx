import { Input, message, Modal, Radio } from "antd";
import Form from "antd/es/form/Form";
import TextArea from "antd/es/input/TextArea";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect } from "react";

const ClassUpdate = ({ open, setOpen, id, reload }) => {
  const [form] = Form.useForm();
  const { REACT_APP_API: BASE_URL } = process.env;

  const getData = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/classes/${id}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      const res = await data.data;
      console.log(res);
      form.setFieldsValue({
        name: res.name,
        description: res.description,
        active: res.active === true ? 1 : 0,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (open) {
      getData();
    }
  }, [open]);

  // !--------------HANDLE--------------
  const handleSubmit = async () => {
    const values = await form.validateFields();
    try {
      const { data } = await axios.put(`${BASE_URL}/classes/${id}`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      message.success(data.message);
      reload();
      setOpen(false);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <Modal open={open} okText="Submit" onOk={handleSubmit} onCancel={() => setOpen(false)} closable={false}>
      <Form form={form} layout="vertical">
        <Form.Item name="name" label="Name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Description" rules={[{ required: true }]}>
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item name="active" label="active">
          <Radio.Group>
            <Radio value={0}>No</Radio>
            <Radio value={1}>Yes</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ClassUpdate;
