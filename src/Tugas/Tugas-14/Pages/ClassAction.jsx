import { Button, Card, message, Popconfirm } from "antd";
import { DeleteTwoTone, EditTwoTone, EyeTwoTone } from "@ant-design/icons";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import AddClass from "./AddClass";
import "./ClassAction.css";
import Meta from "antd/es/card/Meta";
import { useNavigate } from "react-router-dom";
import ClassUpdate from "./ClassUpdate";
import Navigation from "../Components/Navigation";

const ClassAction = () => {
  const [classes, setClasses] = useState([]);
  const [isAddOpen, setIsAddOpen] = useState(false);
  const [isUpdateOpen, setIsUpdateOpen] = useState(false);
  const [idUser, setIdUser] = useState();
  const [reload, setReload] = useState(false);
  const { REACT_APP_API: BASE_URL } = process.env;
  const navigate = useNavigate();
  // !----------------------------------------------------
  const getClass = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/classes?page=1&limit=100`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      setClasses(data.data.data);
    } catch (error) {
      console.log(error);
    }
  };
  const setLoading = () => {
    setReload(!reload);
  };
  const openAdd = () => {
    setIsAddOpen(!isAddOpen);
  };
  const openUpdate = (id) => {
    // console.log(id);
    setIdUser(id);
    setIsUpdateOpen(!isUpdateOpen);
  };
  useEffect(() => {
    getClass();
  }, [reload]);

  // ** -------------------Handle------------------
  const handleDelete = async (id) => {
    try {
      const data = await axios.delete(`${BASE_URL}/classes/${id}`, {
        headers: { Authorization: "Bearer " + Cookies.get("token") },
        data: { deleteImageOnly: false },
      });
      setLoading();
      message.success(data.data.message);
    } catch (error) {
      console.log(error);
    }
  };
  const handleDetail = async (id) => {
    console.log(id);
    navigate(`/classes/${id}`);
  };
  // !-------------------Card-----------------------
  return (
    <>
      <Navigation />
      <div className="class">
        <Button type="primary" onClick={openAdd}>
          Add Class
        </Button>
        <div className="card-wrapper">
          {classes.map((kelas) => {
            return (
              <Card
                className="card-item"
                key={kelas.id}
                actions={[
                  <Popconfirm title="Yakin ingin menghapus kelas?" okText="Ya" cancelText="Tidak" onConfirm={() => handleDelete(kelas.id)}>
                    <DeleteTwoTone twoToneColor="red" key="delete" />
                  </Popconfirm>,
                  <EditTwoTone twoToneColor="#52c41a" key="edit" onClick={() => openUpdate(kelas.id)} />,
                  <EyeTwoTone key="detail" onClick={() => handleDetail(kelas.id)} />,
                ]}
              >
                <Meta title={kelas.name} description={kelas.description} />
              </Card>
            );
          })}
        </div>
        <AddClass open={isAddOpen} setOpen={openAdd} reload={setLoading} />
        <ClassUpdate open={isUpdateOpen} setOpen={openUpdate} id={idUser} reload={setLoading} />
      </div>
    </>
  );
};

export default ClassAction;
