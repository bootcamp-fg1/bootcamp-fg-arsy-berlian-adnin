import { Button, message, Space, Table } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Navigation from "../../Components/Navigation";
import "./User.css";

const User = () => {
  const [data, setData] = useState([]);
  const [reload, setReload] = useState(false);
  const { REACT_APP_API: BASE_URL } = process.env;
  const idClass = "805275f8-06f0-486f-b528-2b1b9ac3090c";
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  //   -----------------------------
  const getData = async () => {
    try {
      const { data } = await axios.get(
        `${BASE_URL}/users?page=1&limit=10&keyword=&role=&classId=${idClass}`,

        { headers: { Authorization: "bearer " + Cookies.get("token") } }
      );
      setData(data.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, [reload]);

  //   *-------------------HANDLE-------------------
  const handleDetail = (id) => {
    navigate(`/users/${id}`);
  };
  const handleDelete = async (id) => {
    setLoading(true);
    try {
      const del = await axios.delete(`${BASE_URL}/users/${id}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      setReload(!reload);
      message.success(del.data.message);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };
  const handleAdd = () => {
    navigate("/users/add");
  };
  const handleUpdate = (id) => {
    navigate(`/users/update/${id}`);
  };
  //   !----------------TABLE------------------------
  const columns = [
    {
      title: "No",
      dataIndex: "index",
      width: 80,
    },
    {
      title: "Name",
      dataIndex: "name",
      width: 150,
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 120,
    },
    {
      title: "Role",
      dataIndex: "role",
      width: 120,
    },
    {
      title: "Action",
      dataIndex: "id",
      render: (x) => (
        <Space>
          <Button loading={loading} className="btn-delete" onClick={() => handleDelete(x)}>
            Delete
          </Button>
          <Button className="btn-update" onClick={() => handleUpdate(x)}>
            Update
          </Button>
          <Button className="btn-detail" onClick={() => handleDetail(x)}>
            Detail
          </Button>
        </Space>
      ),
    },
  ];
  const dataSource = data.map((data, index) => {
    return {
      ...data,
      key: data.id,
      index: index + 1,
    };
  });
  return (
    <>
      <Navigation />
      <div className="user">
        <Button type="primary" onClick={handleAdd}>
          create user
        </Button>
        <Table className="table" columns={columns} dataSource={dataSource} />
      </div>
    </>
  );
};

export default User;
