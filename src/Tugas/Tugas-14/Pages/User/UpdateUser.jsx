import { Button, Form, Input, message, Radio } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const UpdateUser = () => {
  const [form] = Form.useForm();
  const { REACT_APP_API: BASE_URL } = process.env;
  const { id_user } = useParams();
  const navigate = useNavigate();

  const getData = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/users/${id_user}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      //   await setUserData(data.data);
      const res = await data.data;
      form.setFieldsValue({
        ktp: res.ktp,
        name: res.name,
        email: res.email,
        phone: res.phone,
        role: res.role,
        is_Verified: res.is_verified === true ? 1 : 0,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  //   !---------------HANDLE---------------
  const handleSubmit = async () => {
    const values = await form.validateFields();
    try {
      const data = await axios.put(`${BASE_URL}/users/${id_user}`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      navigate("/users");
      message.success(data.data.message);
    } catch (error) {
      if (error.message === "Request failed with status code 422") {
        alert("email harus unik");
      } else {
        alert("gagal update data!");
      }

      console.log(error);
    }
  };
  return (
    <>
      <br />
      <br />
      <div className="add-user">
        <Form form={form} layout="vertical">
          <Form.Item name="ktp" label="KTP">
            <Input />
          </Form.Item>
          <Form.Item name="name" label="Name">
            <Input />
          </Form.Item>
          <Form.Item name="email" label="Email">
            <Input />
          </Form.Item>
          <Form.Item name="phone" label="Phone">
            <Input />
          </Form.Item>
          <Form.Item name="role" label="Role">
            <Input />
          </Form.Item>
          {/* <Form.Item name="password" label="Password">
          <Input.Password />
        </Form.Item>
        <Form.Item name="password_confirmation" label="Password confirm">
          <Input.Password />
        </Form.Item> */}
          <Form.Item name="is_Verified" label="Verified">
            <Radio.Group>
              <Radio value={0}>No</Radio>
              <Radio value={1}>Yes</Radio>
            </Radio.Group>
          </Form.Item>
          <Button type="primary" onClick={handleSubmit} block className="btn-add">
            Update
          </Button>
        </Form>
      </div>
    </>
  );
};

export default UpdateUser;
