import { Button, Form, Input, message, Radio } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React from "react";
import { useNavigate } from "react-router-dom";
import Navigation from "../../Components/Navigation";
import "./AddUser.css";

const AddUser = () => {
  const [form] = Form.useForm();
  const { REACT_APP_API: BASE_URL } = process.env;
  const navigate = useNavigate();

  //   !---------------HANDLE---------------
  const handleSubmit = async () => {
    const values = await form.validateFields();
    try {
      const data = await axios.post(`${BASE_URL}/users`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      alert(data.data.message);
      navigate("/users");
      //   message.success(data.data.message);
      console.log(data);
    } catch (error) {
      alert(error);
      console.log(error);
    }
    // console.log(values);
  };
  return (
    <>
      <Navigation />
      <div className="add-user">
        <Form form={form} layout="vertical">
          <Form.Item name="ktp" label="KTP" rules={[{ required: true }, { min: 10 }, { pattern: new RegExp("^([0-9]*)$"), message: "hanya masukan angka" }]}>
            <Input />
          </Form.Item>
          <Form.Item name="name" label="Name" rules={[{ required: true }, { min: 14 }]}>
            <Input />
          </Form.Item>
          <Form.Item name="email" label="Email" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="phone" label="Phone" rules={[{ required: true }, { min: 10 }, { pattern: new RegExp("^([0-9]*)$"), message: "hanya masukan angka" }]}>
            <Input />
          </Form.Item>
          <Form.Item name="role" label="Role" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="password" label="Password" rules={[{ required: true }, { min: 6 }]}>
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="password_confirmation"
            label="Password confirm"
            rules={[
              { required: true },
              { min: 6 },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error("Password harus sama"));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item name="isVerified" label="Verified">
            <Radio.Group>
              <Radio value={0}>No</Radio>
              <Radio value={1}>Yes</Radio>
            </Radio.Group>
          </Form.Item>
          <Button className="btn-add" type="primary" onClick={handleSubmit} block>
            Submit
          </Button>
        </Form>
      </div>
    </>
  );
};

export default AddUser;
