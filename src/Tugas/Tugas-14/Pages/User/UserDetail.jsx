import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Navigation from "../../Components/Navigation";

const UserDetail = () => {
  const { id_user } = useParams();
  const [user, setUser] = useState([]);
  const { REACT_APP_API: BASE_URL } = process.env;

  const getData = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/users/${id_user}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      setUser(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <Navigation />
      <div className="detail-wrapper">
        <h2>Nama: {user.name}</h2>
        <p>Email: {user.email}</p>
        <p>KTP: {user.ktp}</p>
        <p>Phone: {user.phone}</p>
        <p>Role: {user.role}</p>
      </div>
    </div>
  );
};

export default UserDetail;
