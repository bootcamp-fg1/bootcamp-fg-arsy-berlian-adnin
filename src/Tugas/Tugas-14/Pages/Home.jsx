import React from "react";
import { useNavigate } from "react-router-dom";
import Navigation from "../Components/Navigation";
import "./Home.css";

const Home = () => {
  const navigate = useNavigate();
  return (
    <>
      <Navigation />
      <div className="home">
        <div className="card" onClick={() => navigate("/classes")}>
          <span></span>
          <h1>Class</h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, eveniet. Deleniti illo ipsa molestias temporibus cumque maxime.</p>
        </div>
        <div className="card" onClick={() => navigate("/users")}>
          <span></span>
          <h1>Users</h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, eveniet. Deleniti illo ipsa molestias temporibus cumque maxime.</p>
        </div>
      </div>
    </>
  );
};

export default Home;
