import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ClassAction from "../Pages/ClassAction";
import ClassDetail from "../Pages/ClassDetail";
import Home from "../Pages/Home";
import Login from "../Pages/Login";
import AddUser from "../Pages/User/AddUser";
import UpdateUser from "../Pages/User/UpdateUser";
import User from "../Pages/User/User";
import UserDetail from "../Pages/User/UserDetail";
// import Navigation from "./Navigation";

const RoutePage = () => {
  return (
    <BrowserRouter>
      {/* <Navigation /> */}
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<Home />} />
        <Route path="/classes" element={<ClassAction />} />
        <Route path="/classes/:id_class" element={<ClassDetail />} />
        <Route path="/users" element={<User />} />
        <Route path="/users/:id_user" element={<UserDetail />} />
        <Route path="/users/add" element={<AddUser />} />
        <Route path="/users/update/:id_user" element={<UpdateUser />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RoutePage;
