import React from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css";

const Navigation = () => {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to={"/"}>Dashboard</NavLink>
        </li>
        <li>
          <NavLink to={"/classes"}>Classes</NavLink>
        </li>
        <li>
          <NavLink to={"/users"}>Users</NavLink>
        </li>
        <li>
          <NavLink to={"/login"}>Logout</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
