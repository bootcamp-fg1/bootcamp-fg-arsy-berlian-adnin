import React from "react";
import axios from "axios";
import { useEffect } from "react";
import { Button, Space, Table } from "antd";
import "./TablePage.css";
import { useState } from "react";

const TablePage = () => {
  const [dataTable, setDataTable] = useState([]);
  const [dataPage, setDataPage] = useState({
    page: 1,
    per_page: 6,
    total: 12,
  });

  const getData = async () => {
    try {
      const { data } = await axios.get(`https://reqres.in/api/users?page=${dataPage.page}&per_page=${dataPage.per_page}&total=${dataPage.total}`);
      setDataTable(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, [dataPage]);

  // !------------------------DATA TABLE------------------------------
  const columns = [
    {
      title: "No",
      dataIndex: "index",
      width: 80,
    },
    {
      title: "First Name",
      dataIndex: "first_name",
      width: 150,
    },
    {
      title: "Last Name",
      dataIndex: "last_name",
      width: 150,
    },
    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "Action",
      dataIndex: "id",
      render: (x) => (
        <Space>
          <Button className="btn-delete" onClick={() => alert("data dihapus!")}>
            Delete
          </Button>
          <Button className="btn-update" onClick={() => alert("data diupdate!")}>
            Update
          </Button>
        </Space>
      ),
    },
  ];
  const dataSource = dataTable.map((data, index) => {
    return {
      ...data,
      key: data.id,
      index: index + 1,
    };
  });

  return (
    <div className="table-page">
      <h2>Table User</h2>
      <div className="cover"></div>
      <Table
        className="my-table"
        columns={columns}
        dataSource={dataSource}
        rowClassName={(record) => (record.first_name === "Emma" ? "select-name" : "")}
        pagination={{
          pageSize: dataPage.per_page,
          current: dataPage.page,
          total: dataPage.total,
          showSizeChanger: true,
          pageSizeOptions: [6, 10, 20],

          onShowSizeChange: (page, per_page) =>
            setDataPage((curr) => {
              return {
                ...curr,
                page: page,
                per_page: per_page,
              };
            }),
          onChange: (page) =>
            setDataPage((curr) => {
              return {
                ...curr,
                page: page,
              };
            }),
        }}
      />
    </div>
  );
};

export default TablePage;
