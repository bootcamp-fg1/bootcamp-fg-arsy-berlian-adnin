import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { SolutionOutlined, HomeOutlined, AppstoreOutlined, UsergroupAddOutlined, MenuOutlined, LogoutOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import Sider from "antd/es/layout/Sider";
import { Content, Header } from "antd/es/layout/layout";
import "./LayoutClasses.css";
import Cookies from "js-cookie";

const LayoutClasses = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false);
  const { id_class } = useParams();
  const navigate = useNavigate();
  const name = Cookies.get("class_name");

  const items = [
    { key: "/classes", label: "Home", icon: <HomeOutlined /> },
    { key: "dashboard", label: name, icon: <AppstoreOutlined /> },
    { key: "members", label: "Member", icon: <UsergroupAddOutlined /> },
    { key: "material", label: "Material", icon: <SolutionOutlined /> },
    { key: "logout", label: "Logout", icon: <LogoutOutlined /> },
  ];

  // !----------HANDLE---------------------------------------
  const handleNavigate = (key) => {
    if (key === "/classes") {
      navigate(key);
    } else if (key === "logout") {
      navigate("/");
    } else {
      navigate(`/classes/${id_class}/${key}`);
    }
  };
  return (
    <Layout className="lay-dash">
      <Sider className="lay-sider" theme="light" trigger={null} collapsible collapsed={collapsed} width={300}>
        <div className="lay-logo">
          <div className="title-wrapper">
            <div className="circle"></div>
            <h2>AB Bootcamp</h2>
          </div>
        </div>
        <Menu onClick={({ key }) => handleNavigate(key)} theme="light" mode="inline" defaultSelectedKeys={["dashboard"]} items={items} />
      </Sider>
      <Layout className="site-layout">
        <Header className="lay-header">
          {React.createElement(MenuOutlined, {
            className: "trigger",
            onClick: () => setCollapsed(!collapsed),
          })}
        </Header>
        <Content className="lay-content">{children}</Content>
      </Layout>
    </Layout>
  );
};

export default LayoutClasses;
