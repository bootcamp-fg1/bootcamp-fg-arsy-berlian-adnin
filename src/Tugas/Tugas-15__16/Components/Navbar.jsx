import { Button, Dropdown } from "antd";
import Cookies from "js-cookie";
import React from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css";
import { UserOutlined } from "@ant-design/icons";
const items = [
  {
    key: "1",
    label: <NavLink to={"/"}>Logout</NavLink>,
  },
];
const Navbar = () => {
  const name = Cookies.get("name");
  return (
    <nav>
      <ul>
        <li>
          <NavLink to={"/home"}>Berlian Bootcamp</NavLink>
        </li>

        <li>
          <Dropdown
            trigger={["click"]}
            menu={{
              items,
            }}
            placement="bottomLeft"
            arrow={{
              pointAtCenter: true,
            }}
          >
            <Button onClick={(e) => e.preventDefault()} className="btn-logout">
              <UserOutlined /> {name}
            </Button>
          </Dropdown>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
