import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LayoutClasses from "../Layout/LayoutClasses";
import ClassDetail from "../Pages/Classes/Detail/Class-Dashboard/ClassDetail";
import Classes from "../Pages/Classes/Classes";
import Login from "../Pages/Login/Login";
import Members from "../Pages/Classes/Detail/Member-Dashboard/Members";
import RequireLogin from "./RequireLogin";
import Materials from "../Pages/Classes/Detail/Materials-Dashboard/Materials";
import AddMaterial from "../Pages/Classes/Detail/Materials-Dashboard/AddMaterial";
import DetailMaterial from "../Pages/Classes/Detail/Materials-Dashboard/DetailMaterial";

const RoutePage = () => {
  return (
    <BrowserRouter>
      <Routes>
        {/* Login----- */}
        <Route path="/" element={<Login />} />
        {/* Classes---- */}
        <Route
          path="/classes"
          element={
            <RequireLogin>
              <Classes />
            </RequireLogin>
          }
        />
        <Route
          path="/classes/:id_class/dashboard"
          element={
            <RequireLogin>
              <LayoutClasses>
                <ClassDetail />
              </LayoutClasses>
            </RequireLogin>
          }
        />
        {/* Members------ */}
        <Route
          path="/classes/:id_class/members"
          element={
            <RequireLogin>
              <LayoutClasses>
                <Members />
              </LayoutClasses>
            </RequireLogin>
          }
        />
        {/* Material------ */}
        <Route
          path="/classes/:id_class/material"
          element={
            <RequireLogin>
              <LayoutClasses>
                <Materials />
              </LayoutClasses>
            </RequireLogin>
          }
        />
        <Route
          path="/classes/:id_class/material_add"
          element={
            <RequireLogin>
              <LayoutClasses>
                <AddMaterial />
              </LayoutClasses>
            </RequireLogin>
          }
        />
        <Route
          path="/classes/:id_class/material/:id_material"
          element={
            <RequireLogin>
              <LayoutClasses>
                <DetailMaterial />
              </LayoutClasses>
            </RequireLogin>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};

export default RoutePage;
