import Cookies from "js-cookie";
import React, { Fragment } from "react";
import { Navigate } from "react-router-dom";

const RequireLogin = ({ children }) => {
  if (!Cookies.get("token")) return <Navigate to="/" />;
  return <Fragment>{children}</Fragment>;
};

export default RequireLogin;
