import { LoadingOutlined } from "@ant-design/icons";
import { Form, Input, message, Modal, Radio, Spin } from "antd";
import TextArea from "antd/es/input/TextArea";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useState } from "react";

const AddClass = ({ open, setOpen, reload }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();
  const handleSubmit = async () => {
    setIsLoading(true);
    const values = await form.validateFields();
    try {
      const { data } = await axios.post(`https://thankful-calf-bonnet.cyclic.app/api/classes`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      message.success(data.message);
      reload();
      form.resetFields();
      setOpen(false);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const handlerCancle = () => {
    setOpen(false);
    form.resetFields();
  };
  //   ---------------------
  // -----------ant--------
  const antIcon = (
    <LoadingOutlined
      style={{
        fontSize: 24,
        color: "#1A355F",
      }}
      spin
    />
  );
  return (
    <Modal open={open} okText="Submit" onOk={handleSubmit} onCancel={handlerCancle} closable={false}>
      {isLoading ? (
        <Spin indicator={antIcon} />
      ) : (
        <Form form={form} layout="vertical">
          <h2 className="title-modal-class">Add Class</h2>
          <Form.Item name="name" label="Name" rules={[{ required: true }, { min: 5 }]}>
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description" rules={[{ required: true }, { min: 10 }]}>
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item name="active" label="active" rules={[{ required: true }]}>
            <Radio.Group>
              <Radio value={0}>No</Radio>
              <Radio value={1}>Yes</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      )}
    </Modal>
  );
};

export default AddClass;
