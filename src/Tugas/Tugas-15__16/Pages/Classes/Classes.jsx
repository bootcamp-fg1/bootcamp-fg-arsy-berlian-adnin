import { Button, Card, message, Popconfirm } from "antd";
import { DeleteTwoTone, EditTwoTone, EyeTwoTone } from "@ant-design/icons";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import AddClass from "./AddClass";
import "./Classes.css";
import Meta from "antd/es/card/Meta";
import { useNavigate } from "react-router-dom";
import ClassUpdate from "./ClassUpdate";
import Navbar from "../../Components/Navbar";
import { ColorRing } from "react-loader-spinner";

const Classes = () => {
  const [classes, setClasses] = useState([]);
  const [isAddOpen, setIsAddOpen] = useState(false);
  const [isUpdateOpen, setIsUpdateOpen] = useState(false);
  const [idUser, setIdUser] = useState();
  const [reload, setReload] = useState(false);
  const [isSpiner, setIsSpiner] = useState(false);
  const { REACT_APP_API: BASE_URL } = process.env;
  const navigate = useNavigate();
  const roles = Cookies.get("role");

  // !----------------------------------------------------
  const getClass = async () => {
    setIsSpiner(true);
    try {
      const { data } = await axios.get(`${BASE_URL}/classes?page=1&limit=100`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      setClasses(data.data.data);
    } catch (error) {
      console.log(error);
    }
    setIsSpiner(false);
  };
  const setLoading = () => {
    setReload(!reload);
  };
  const openAdd = () => {
    setIsAddOpen(!isAddOpen);
  };
  const openUpdate = (id) => {
    setIdUser(id);
    setIsUpdateOpen(!isUpdateOpen);
  };

  useEffect(() => {
    getClass();
  }, [reload]);

  // ** -------------------Handle------------------
  const handleDelete = async (id) => {
    try {
      const data = await axios.delete(`${BASE_URL}/classes/${id}`, {
        headers: { Authorization: "Bearer " + Cookies.get("token") },
        data: { deleteImageOnly: false },
      });
      setLoading();
      message.success(data.data.message);
    } catch (error) {
      console.log(error);
    }
  };
  const handleDetail = async (id, name) => {
    console.log(id);
    Cookies.set("class_name", name);
    navigate(`/classes/${id}/dashboard`);
  };
  // !-------------------Card-----------------------
  return (
    <>
      {roles === "student" ? console.log("ini student") : console.log("bukan student")}
      <Navbar />
      <div className="class">
        <>
          {isSpiner ? (
            <ColorRing visible={true} height="80" width="80" ariaLabel="blocks-loading" wrapperStyle={{}} wrapperClass="blocks-wrapper" colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]} />
          ) : (
            <>
              {roles !== "student" && (
                <Button className="btn-add-class" onClick={openAdd}>
                  Add Class
                </Button>
              )}
              <div className="card-wrapper">
                {roles === "student" ? (
                  <>
                    {classes.map((kelas) => {
                      return (
                        <Card style={{ cursor: "pointer" }} onClick={() => handleDetail(kelas.id, kelas.name)} className="card-item" key={kelas.id}>
                          <Meta title={kelas.name} description={kelas.description} />
                        </Card>
                      );
                    })}
                  </>
                ) : (
                  <>
                    {classes.map((kelas) => {
                      return (
                        <Card
                          style={{ cursor: "pointer" }}
                          onClick={() => handleDetail(kelas.id, kelas.name)}
                          className="card-item"
                          key={kelas.id}
                          actions={[
                            <Popconfirm title="Yakin ingin menghapus kelas?" okText="Ya" cancelText="Tidak" onConfirm={() => handleDelete(kelas.id)}>
                              <DeleteTwoTone twoToneColor="#f47522" key="delete" />
                            </Popconfirm>,
                            <EditTwoTone twoToneColor="#f47522" key="edit" onClick={() => openUpdate(kelas.id)} />,
                            <EyeTwoTone twoToneColor="#f47522" key="detail" onClick={() => handleDetail(kelas.id, kelas.name)} />,
                          ]}
                        >
                          <Meta title={kelas.name} description={kelas.description} />
                        </Card>
                      );
                    })}
                  </>
                )}
              </div>
              <AddClass open={isAddOpen} setOpen={openAdd} reload={setLoading} />
              <ClassUpdate open={isUpdateOpen} setOpen={openUpdate} id={idUser} reload={setLoading} />
            </>
          )}
        </>
      </div>
    </>
  );
};

export default Classes;
