import { LoadingOutlined } from "@ant-design/icons";
import { Input, message, Modal, Radio, Spin } from "antd";
import Form from "antd/es/form/Form";
import TextArea from "antd/es/input/TextArea";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";

const ClassUpdate = ({ open, setOpen, id, reload }) => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);
  const { REACT_APP_API: BASE_URL } = process.env;

  const getData = async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(`${BASE_URL}/classes/${id}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      const res = await data.data;
      console.log(res);
      form.setFieldsValue({
        name: res.name,
        description: res.description,
        active: res.active === true ? 1 : 0,
      });
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (open) {
      getData();
    }
  }, [open]);

  // !--------------HANDLE--------------
  const handleSubmit = async () => {
    setIsLoading(true);
    const values = await form.validateFields();
    try {
      const { data } = await axios.put(`${BASE_URL}/classes/${id}`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      message.success(data.message);
      reload();
      setOpen(false);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };
  // ---------------ant----------------
  const antIcon = (
    <LoadingOutlined
      style={{
        fontSize: 24,
        color: "#1A355F",
      }}
      spin
    />
  );
  return (
    <Modal open={open} okText="Submit" onOk={handleSubmit} onCancel={() => setOpen(false)} closable={false}>
      {isLoading ? (
        <Spin indicator={antIcon} />
      ) : (
        <Form form={form} layout="vertical">
          <h2 className="title-modal-class">Update Class</h2>
          <Form.Item name="name" label="Name" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description" rules={[{ required: true }]}>
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item name="active" label="active" rules={[{ required: true }]}>
            <Radio.Group>
              <Radio value={0}>No</Radio>
              <Radio value={1}>Yes</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      )}
    </Modal>
  );
};

export default ClassUpdate;
