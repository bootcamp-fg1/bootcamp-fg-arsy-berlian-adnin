import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const DetailMaterial = () => {
  const { REACT_APP_API: BASE_URL } = process.env;
  const [dataDetail, setDataDetail] = useState([]);
  const { id_class, id_material } = useParams();
  console.log(id_material);

  const getData = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/materials/${id_material}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });

      setDataDetail(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  //   convert date
  function datexxx(value) {
    let unix_timestamp = value;
    var date = new Date(unix_timestamp);
    var tanggal = date.getDate();
    var mL = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
    var bulan = mL[date.getMonth()];
    var tahun = date.getFullYear();
    var formattedTime = tanggal + "  " + bulan + "  " + tahun;
    return formattedTime;
  }
  return (
    <div className="material-detail">
      <div className="material-detail-wrapper">
        <h1>{dataDetail.name}</h1>
        <p>{dataDetail.description}</p>

        <p>published : {datexxx(dataDetail.published)}</p>

        <p dangerouslySetInnerHTML={{ __html: JSON.stringify(dataDetail.material_1) }}></p>
      </div>
    </div>
  );
};

export default DetailMaterial;
