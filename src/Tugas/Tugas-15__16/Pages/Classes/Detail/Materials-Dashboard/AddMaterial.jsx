import { Button, DatePicker, Form, Input, Select, message } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const AddMaterial = () => {
  const navigate = useNavigate();
  const [ownRole, setOwnRole] = useState([]);
  const { id_class } = useParams();
  const [form] = Form.useForm();
  const { REACT_APP_API: BASE_URL } = process.env;

  const getDataUser = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/users?page=1&limit=100&keyword=&role=&classId=${id_class}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      const userData = data.data.data.filter((x) => x.role === "trainer" || x.role === "admin");
      setOwnRole(userData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getDataUser();
  }, []);

  //   filter role
  const options = ownRole.map((role) => ({
    value: role.id,
    label: role.name,
  }));
  // !----------------HANDLER---------------
  const handleSubmit = async () => {
    const values = await form.validateFields();
    const dataVal = await {
      ...values,
      classId: id_class,
      material_1: { "<b>": "ini adalah JSON dari html, material_2 & material_3 optional" },
      published: values.published.format("YYYY-MM-DD HH:mm:ss"),
      //   ownerId: "4726ded3-717d-4730-a980-04c52f00b773",
      //   ownerId: "01d8c964-a72d-4a35-95f9-da664e89e9f7",
    };
    try {
      const { data } = await axios.post(`${BASE_URL}/materials/`, dataVal, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      message.success("berhasil menambahkan data!");
      console.log("ini data post", data);
      navigate(-1);
    } catch (error) {
      console.log("kok gagal???", error);
    }
    console.log("ini data value:", dataVal);
  };

  return (
    <div>
      <Form layout="vertical" form={form} style={{ width: "500px" }}>
        {/* <Form.Item name="classId" label="class id">
          <Input disabled />
        </Form.Item> */}
        <Form.Item name="name" label="name" rules={[{ required: true }, { min: 5 }]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label="description" rules={[{ required: true }, { min: 10 }]}>
          <Input />
        </Form.Item>
        <Form.Item name="material_1" label="material 1">
          <Input />
        </Form.Item>
        <Form.Item name="status" label="status" rules={[{ required: true, message: "status tidak boleh kosong!" }]}>
          <Select
            style={{
              width: 300,
            }}
            options={[
              { value: "published", label: "published" },
              { value: "scheduled", label: "scheduled" },
              { value: "draft", label: "draft" },
            ]}
          />
        </Form.Item>
        <Form.Item name="published" label="published">
          <DatePicker showTime />
        </Form.Item>
        <Form.Item name="label" label="label">
          <Input />
        </Form.Item>
        <Form.Item name="ownerId" label="owner id">
          <Select options={options} />
        </Form.Item>

        <Button onClick={handleSubmit}>Submit</Button>
      </Form>
    </div>
  );
};

export default AddMaterial;
