import { Button, Form, Input, Popconfirm, Select, Space, Table } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { DeleteOutlined, FormOutlined, EyeOutlined } from "@ant-design/icons";
import "./Material.css";
import { ColorRing } from "react-loader-spinner";

const Materials = () => {
  const [dataPage, setDataPage] = useState({
    page: 1,
    per_page: 5,
    total: 12,
  });
  const [isLoading, setIsLoading] = useState(false);
  const [isReload, setIsReload] = useState(false);
  const navigate = useNavigate();
  const [dataTable, setDataTable] = useState([]);
  const { id_class } = useParams();
  const { REACT_APP_API: BASE_URL } = process.env;
  const [keyword, setKeyword] = useState("");
  const [labels, setLabels] = useState([]);
  const [keyLabel, setKeyLabel] = useState("");
  const roles = Cookies.get("role");
  const getData = async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(`${BASE_URL}/materials?page=${dataPage.page}&limit=${dataPage.per_page}&keyword=${keyword}&classId=${id_class}&label=${keyLabel}&ownerId=`, {
        headers: { Authorization: "bearer " + Cookies.get("token") },
      });

      setDataTable(data.data.data);
      setLabels(data.labels);
      console.log("data===>", data.labels);
    } catch (error) {
      console.log("pesan error: ", error);
    }
    setIsLoading(false);
  };

  const refreshPage = () => {
    setIsReload(!isReload);
  };
  useEffect(() => {
    getData();
  }, [isReload, keyword, keyLabel]);
  // !-----------------------handler--------------
  const handleDelete = async (id) => {
    try {
      const { data } = await axios.delete(`${BASE_URL}/materials/${id}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      refreshPage();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };
  const handleToDetail = (id) => {
    navigate(`/classes/${id_class}/material/${id}`);
  };
  //   ?----------------------ant atribut--------------
  const columnsNotStudent = [
    {
      title: "No",
      dataIndex: "index",
      width: 70,
    },
    {
      title: "Name",
      dataIndex: "name",
      width: 270,
    },
    {
      title: "Description",
      dataIndex: "description",
      width: 370,
    },
    {
      title: "Status",
      dataIndex: "status",
      width: 270,
    },
    {
      title: "Action",
      dataIndex: "id",
      render: (x) => (
        <Space>
          <Popconfirm title="Yakin ingin menghapus data?" okText="Ya" cancelText="Tidak" onConfirm={() => handleDelete(x)}>
            <Button className="btn-delete">
              <DeleteOutlined />
            </Button>
          </Popconfirm>
          <Button className="btn-update" onClick={() => alert("lagi ada kegiatan blm sempet buat update")}>
            <FormOutlined />
          </Button>
          <Button className="btn-detail" onClick={() => handleToDetail(x)}>
            <EyeOutlined />
          </Button>
        </Space>
      ),
    },
  ];
  const columnsStudent = [
    {
      title: "No",
      dataIndex: "index",
      width: 70,
    },
    {
      title: "Name",
      dataIndex: "name",
      width: 270,
    },
    {
      title: "Description",
      dataIndex: "description",
      width: 370,
    },
    {
      title: "Status",
      dataIndex: "status",
      width: 270,
    },
    {
      title: "Action",
      dataIndex: "id",
      render: (x) => (
        <Space>
          <Button className="btn-detail" onClick={() => handleToDetail(x)}>
            <EyeOutlined />
          </Button>
        </Space>
      ),
    },
  ];

  const dataSource = dataTable.map((data, index) => {
    return {
      ...data,
      key: data.id,
      index: index + 1,
    };
  });
  console.log(labels.label);
  const options = labels.map((x) => {
    return {
      value: x.label,
      name: x.label,
    };
  });
  //   !---------------handler---------------
  const handlerToAddMaterial = () => {
    navigate(`/classes/${id_class}/material_add`);
  };
  const handleSearch = (e) => {
    setKeyword(e);
  };
  return (
    <div>
      <Form layout="vertical">
        <Form.Item name="labels" label="label">
          <Select options={options} onSelect={(e) => setKeyLabel(e)} />
        </Form.Item>
      </Form>
      <Input onChange={(e) => handleSearch(e.target.value)} placeholder="search material" style={{ marginBottom: "20px" }} />
      {roles !== "student" && (
        <Button className="btn-add-member" onClick={handlerToAddMaterial}>
          add material
        </Button>
      )}
      {isLoading ? (
        <ColorRing visible={true} height="80" width="80" ariaLabel="blocks-loading" wrapperStyle={{}} wrapperClass="blocks-wrapper" colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]} />
      ) : (
        <Table columns={roles !== "student" ? columnsNotStudent : columnsStudent} dataSource={dataSource} />
      )}
    </div>
  );
};

export default Materials;
