import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import "./ClassDetail.css";
import { ColorRing } from "react-loader-spinner";
// --

const ClassDetail = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { id_class } = useParams();
  const [detail, setDetail] = useState([]);
  const { REACT_APP_API: BASE_URL } = process.env;

  const getData = async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(`${BASE_URL}/classes/${id_class}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      await setDetail(data.data);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    getData();
  }, []);

  // -----------antd-----------

  return (
    <>
      {/* <Navigation /> */}
      <div className="detail-class">
        {isLoading ? (
          <ColorRing visible={true} height="80" width="80" ariaLabel="blocks-loading" wrapperStyle={{}} wrapperClass="blocks-wrapper" colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]} />
        ) : (
          <div className="detail-wrapper">
            <img src="https://academy.alterra.id/wp-content/uploads/2021/07/dashboard.svg" alt="" />
            <h1>{detail.name}</h1>
            <p>{detail.description}</p>
          </div>
        )}
      </div>
    </>
  );
};

export default ClassDetail;
