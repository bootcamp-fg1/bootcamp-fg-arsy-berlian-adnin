import { Button, Form, Input, Select, message } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

const AddMember = ({ reload, closeBtn }) => {
  const { REACT_APP_API: BASE_URL } = process.env;
  const { id_class } = useParams();
  const [dataUser, setDataUser] = useState([]);
  const [form] = Form.useForm();

  const getData = async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/users?page=1&limit=100&keyword=&role=&classId=${id_class}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      setDataUser(data.data.data);
      //   console.log(data.data.data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  // !--------------SELECT----------
  const options = dataUser.map((data) => ({
    value: data.id,
    label: `${data.name} -  (${data.email}) `,
  }));

  //   !-------------HANDLE-----------
  const handleSubmit = async () => {
    const values = await form.validateFields();
    try {
      const { data } = await axios.post(`${BASE_URL}/classes/${id_class}/members`, values, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      form.resetFields();
      reload();
      closeBtn();
      message.success(data.message);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="form-add-member">
      <Form form={form} className="form-add">
        <Form.Item name="userId" rules={[{ required: true, message: "member tidak boleh kosong!" }]}>
          <Select
            showSearch
            style={{
              width: 300,
            }}
            placeholder="Search to Select"
            optionFilterProp="children"
            filterOption={(input, option) => (option?.label ?? "").includes(input)}
            filterSort={(optionA, optionB) => (optionA?.label ?? "").toLowerCase().localeCompare((optionB?.label ?? "").toLowerCase())}
            //   optionLabelProp="halo"
            options={options}
          />
        </Form.Item>
        <Form.Item name="role" rules={[{ required: true, message: "role tidak boleh kosong!" }]}>
          <Select
            style={{
              width: 300,
            }}
            options={[
              { value: "trainer", label: "trainer" },
              { value: "student", label: "student" },
            ]}
          />
        </Form.Item>

        <Button className="btn-add-member-submit" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
    </div>
  );
};

export default AddMember;
