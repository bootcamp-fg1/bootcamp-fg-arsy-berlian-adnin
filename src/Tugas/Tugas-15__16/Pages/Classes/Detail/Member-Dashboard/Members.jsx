import { Button, Popconfirm, Space, Table, message } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./Members.css";
import AddMember from "./AddMember";
import UpdateMember from "./UpdateMember";
import { DeleteOutlined, FormOutlined } from "@ant-design/icons";
import { ColorRing } from "react-loader-spinner";
import Search from "antd/es/transfer/search";

const Members = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [openAdd, setOpenAdd] = useState(false);
  const [openUpdate, setOpenUpdate] = useState(false);
  const [reload, setReload] = useState(false);
  const [role, setRole] = useState([]);
  const { REACT_APP_API: BASE_URL } = process.env;
  const { id_class } = useParams();
  const [members, setMembers] = useState([]);
  const [idMember, setIdMember] = useState([]);
  const [idMemberUpdate, setIdMemberUpdate] = useState([]);
  const [searching, setSearching] = useState("");
  const [dataPage, setDataPage] = useState({
    page: 1,
    per_page: 5,
    total: 12,
  });
  const roles = Cookies.get("role");
  const getData = async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(`${BASE_URL}/classes/${id_class}/members?page=${dataPage.page}&limit=${dataPage.per_page}&keyword=${searching}`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      const userRole = await data.data.data.map((datas) => datas.role);
      setRole(userRole);
      const userMember = await data.data.data.map((datas) => datas.user);
      setMembers(userMember);
      setIdMember(data.data.data);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  // **-----------------data table------------------
  const columnsNotStudent = [
    { title: "No", dataIndex: "index", width: 80 },
    { title: "Email", dataIndex: "email", width: 150 },
    { title: "Name", dataIndex: "name", width: 150 },
    { title: "Role", dataIndex: "role", width: 80 },
    {
      title: "Action",
      dataIndex: "id_mem",
      width: 150,
      render: (x) => (
        <Space>
          <Popconfirm title="Yakin ingin menghapus data?" okText="Ya" cancelText="Tidak" onConfirm={() => handleDelete(x)}>
            <Button className="btn-delete">
              <DeleteOutlined />
            </Button>
          </Popconfirm>
          <Button className="btn-update" onClick={() => handlerOpenUpdate(x)}>
            <FormOutlined />
          </Button>
        </Space>
      ),
    },
  ];
  const columnsStudent = [
    { title: "No", dataIndex: "index", width: 80 },
    { title: "Email", dataIndex: "email", width: 150 },
    { title: "Name", dataIndex: "name", width: 150 },
    { title: "Role", dataIndex: "role", width: 80 },
  ];

  const dataSource = members.map((data, index) => {
    return {
      ...data,
      id_mem: idMember[index].id,
      role: role[index],
      key: data.id,
      index: index + 1,
    };
  });
  //!--------------------HANDLE------------------------
  const handleReload = () => {
    setReload(!reload);
  };
  const handleAdd = () => {
    setOpenAdd(!openAdd);
  };

  const handleDelete = async (id) => {
    try {
      const data = await axios.delete(`${BASE_URL}/members/${id}`, {
        headers: { Authorization: "Bearer " + Cookies.get("token") },
      });
      handleReload();
      message.success(data.data.message);
    } catch (error) {
      console.log(error);
    }
  };

  const handlerOpenUpdate = (id) => {
    setIdMemberUpdate(id);
    setOpenUpdate(!openUpdate);
  };

  const onSearch = (value) => {
    setSearching(value);
  };

  useEffect(() => {
    getData();
  }, [reload, dataPage, searching]);

  return (
    <div className="member">
      <div className="search">
        <Search
          className="load-search"
          placeholder="input search text"
          onChange={(e) => onSearch(e.target.value)}
          style={{
            width: 200,
          }}
        />
      </div>
      {isLoading ? (
        <div className="load-spiner">
          <ColorRing visible={true} height="80" width="80" ariaLabel="blocks-loading" wrapperStyle={{}} wrapperClass="blocks-wrapper" colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]} />
        </div>
      ) : (
        <>
          <div className="member-wrapper">
            {roles !== "student" && (
              <Button className="btn-add-member" onClick={handleAdd}>
                Add Member
              </Button>
            )}
            {openAdd && <AddMember reload={handleReload} closeBtn={handleAdd} />}
            <Table
              className="table-member"
              columns={roles !== "student" ? columnsNotStudent : columnsStudent}
              dataSource={dataSource}
              pagination={{
                pageSize: dataPage.per_page,
                current: dataPage.page,
                // total: dataPage.total,
                showSizeChanger: true,
                pageSizeOptions: [5, 10, 20],

                onShowSizeChange: (page, per_page) =>
                  setDataPage((curr) => {
                    return {
                      ...curr,
                      page: page,
                      per_page: per_page,
                    };
                  }),

                onchange: (page) =>
                  setDataPage((curr) => {
                    return {
                      ...curr,
                      page: page,
                    };
                  }),
              }}
            />
          </div>
        </>
      )}

      <UpdateMember open={openUpdate} setOpen={handlerOpenUpdate} id_member={idMemberUpdate} reload={handleReload} />
    </div>
  );
};

export default Members;
