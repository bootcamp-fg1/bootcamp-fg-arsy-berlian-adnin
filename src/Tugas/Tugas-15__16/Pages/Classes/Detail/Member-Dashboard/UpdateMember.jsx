import { LoadingOutlined } from "@ant-design/icons";
import { Button, Form, Modal, Select, Spin, message } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const UpdateMember = ({ open, setOpen, id_member, reload }) => {
  const { REACT_APP_API: BASE_URL } = process.env;
  const { id_class } = useParams();
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);

  let dataUser = {};
  const getData = async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(`${BASE_URL}/classes/${id_class}/members`, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      const getId = await data.data.data.filter((x) => x.id === id_member);
      const resUser = await getId
        .map((x) => x.user)
        .map((x) => {
          return {
            id: x.id,
            name: x.name,
          };
        });
      const resToObj = resUser.reduce((res, item) => {
        res[item.id] = item;
        return res;
      });
      dataUser = resToObj;

      const name = await getId.map((x) => x.user).map((x) => x.name);
      const role = await getId.map((x) => x.role);
      form.setFieldsValue({
        userId: name,
        role: role,
      });
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (open) {
      getData();
    }
  }, [open]);
  // !--------------SELECT----------
  const options = [
    {
      value: dataUser?.id,
      label: dataUser?.name,
    },
  ];

  //   !-------------HANDLE-----------
  const handleSubmit = async () => {
    const values = await form.validateFields();
    const role = { role: values.role };
    setIsLoading(true);
    try {
      const { data } = await axios.put(`${BASE_URL}/members/${id_member}`, role, { headers: { Authorization: "bearer " + Cookies.get("token") } });
      form.resetFields();
      reload();
      message.success(data.message);
      setOpen();
    } catch (error) {
      console.log(error);
      message.warning("terjasi kesalahan!");
    }
    setIsLoading(false);
  };

  const handlerCancle = () => {
    setOpen();
    form.resetFields();
  };

  // -----------ant--------
  const antIcon = (
    <LoadingOutlined
      style={{
        fontSize: 24,
        color: "#1A355F",
      }}
      spin
    />
  );
  return (
    <Modal open={open} onCancel={handlerCancle} onOk={handleSubmit} okText="Update" closable={false}>
      {isLoading ? (
        <Spin indicator={antIcon} />
      ) : (
        <Form form={form} layout="vertical">
          <h3 className="title-modal-class">update member</h3>
          <Form.Item name="userId" label="name" rules={[{ required: true, message: "member tidak boleh kosong!" }]}>
            <Select
              disabled
              showSearch
              placeholder="Search to Select"
              optionFilterProp="children"
              filterOption={(input, option) => (option?.label ?? "").includes(input)}
              filterSort={(optionA, optionB) => (optionA?.label ?? "").toLowerCase().localeCompare((optionB?.label ?? "").toLowerCase())}
              options={options}
            />
          </Form.Item>
          <Form.Item name="role" label="role" rules={[{ required: true, message: "role tidak boleh kosong!" }]}>
            <Select
              options={[
                { value: "trainer", label: "trainer" },
                { value: "student", label: "student" },
              ]}
            />
          </Form.Item>
        </Form>
      )}
    </Modal>
  );
};

export default UpdateMember;
